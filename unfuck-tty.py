#!/usr/bin/python3

import argparse
import os
from fcntl import ioctl
from enum import IntEnum
from array import array
from sys import exit

class KdCmd(IntEnum):
    SETMODE = 0x4B3A
    GETMODE = 0x4B3B
    GKBMODE = 0x4B44
    SKBMODE = 0x4B45

class KdTtyMode(IntEnum):
    TEXT     = 0x00
    GRAPHICS = 0x01

class KdKbdMode(IntEnum):
    RAW       = 0x00
    XLATE     = 0x01
    MEDIUMRAW = 0x02
    UNICODE   = 0x03
    OFF       = 0x04

def parse_tty_arg(arg):
    try:
        # tty = os.open('/dev/tty%d' % int(arg), os.O_RDWR)
        return open('/dev/tty%d' % int(arg), 'w')
    except ValueError:
        raise argparse.ArgumentTypeError('%s is not a valid integer' % arg)
    except IOError as e:
        raise argparse.ArgumentTypeError('Failed to open /dev/tty%d: %s' % (
            int(arg), e.strerror))

def parse_kbd_mode(arg):
    try:
        if arg.isnumeric():
            return KdKbdMode(int(arg))
        else:
            return KdKbdMode[arg.upper()]
    except (ValueError,KeyError):
        raise argparse.ArgumentTypeError(
            'Invalid kbd mode, must be one of %s' % [e.name.title() for e in KdTtyMode]
        )

def parse_tty_mode(arg):
    try:
        if arg.isnumeric():
            return KdTtyMode(int(arg))
        else:
            return KdTtyMode[arg.upper()]
    except (ValueError,KeyError):
        raise argparse.ArgumentTypeError(
            'Invalid tty mode, must be one of %s' % [e.name.title() for e in KdKbdMode]
        )

parser = argparse.ArgumentParser(
    description=("'Unfuck' a tty out of graphics mode and back into it's "
                 "original state, so that another compositor may take it's "
                 "place."),
)
parser.add_argument(
    'tty',
    help='The tty to reset',
    type=parse_tty_arg
)
parser.add_argument(
    '-k', '--kbd-mode', metavar='MODE',
    help='Keyboard mode to use (must be one of %s or an int)' % (
        [e.name.title() for e in KdKbdMode]
    ),
    default=KdKbdMode.UNICODE, type=parse_kbd_mode
)
parser.add_argument(
    '-K', '--no-kbd',
    help="Don't set the kbd mode of the tty",
    dest='set_kbd_mode', default=True, action='store_false'
)
parser.add_argument(
    '-t', '--tty-mode', metavar='MODE',
    help='TTY mode to use (must be one of %s or an int)' % (
        [e.name.title() for e in KdTtyMode]
    ),
    default=KdTtyMode.TEXT, type=parse_tty_mode
)
parser.add_argument(
    '-T', '--no-tty',
    help="Don't set the graphical mode of the tty",
    dest='set_tty_mode', default=True, action='store_false'
)
parser.add_argument(
    '-g', '--get',
    help="Just print the current tty modes and exit",
    dest='get_only', default=False, action='store_true'
)

args = parser.parse_args()

def int_ioctl(fd, cmd):
    buf = array('h', [0])
    ioctl(fd, cmd, buf, 1)
    return buf[0]

if args.get_only:
    kbd_mode = KdKbdMode(int_ioctl(args.tty, KdCmd.GKBMODE))
    tty_mode = KdTtyMode(int_ioctl(args.tty, KdCmd.GETMODE))

    print('Current keyboard mode is %s (%d)' %
          (kbd_mode.name.title(), kbd_mode.value))
    print('Current tty mode is %s (%d)' %
          (tty_mode.name.title(), tty_mode.value))
    exit(0)

if args.set_kbd_mode:
    ioctl(args.tty, KdCmd.SKBMODE, args.kbd_mode)
if args.set_tty_mode:
    ioctl(args.tty, KdCmd.SETMODE, args.tty_mode)
